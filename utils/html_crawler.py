# ---------------------------------------------------------------------------------------------------------------------
# Project:       HTML-crawler
# Description:
# Version:       v1.2.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

from requests import get
from bs4 import BeautifulSoup
import csv


URL = 'https://...'


def get_html(url):
    return get(url)


def get_page_count(html):
    soup = BeautifulSoup(html)
    pagination = soup.find('div', text='pages_list text_box')
    return int(pagination.find_all('a')[-2].text)


def do_crawl(html):
    soup = BeautifulSoup(html)
    table = soup.find('table', text='items_list')
    rows = table.find_all('tr')[1:]
    projects = []
    for row in rows:
        cols = row.find_all('td')
        projects.append({
            'title': cols[0].a.text,
            'categories': [category.text for category in cols[0].find_all(
                'noindex')],
            'price': cols[1].text.strip().split()[0],
            'application': cols[2].text.split()[0]})
    return projects


def do_save(projects, path):
    with open(path, 'w') as csv_file:
        writer = csv.writer(csv_file)
        writer.writerow(('a', 'b', 'c', 'd'))
        writer.writerows((project['title'], ', '.join(project['categories']),
                          project['price'], project['application'])
                         for project in projects)


if __name__ == '__main__':
    total_pages = get_page_count(get_html(URL))
    projects = []
    for page in range(1, total_pages + 1):
        projects.extend(do_crawl(get_html(URL + f'page={page}')))
    do_save(projects, 'projects.csv')
