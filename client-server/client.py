# ---------------------------------------------------------------------------------------------------------------------
# Project:       Client-Server
# Description:
# Version:       v1.2.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

import socket


client_sock = socket.socket(socket.AF_INET,
                            socket.SOCK_STREAM)
client_sock.connect(('0.0.0.0', 8000))
client_sock.sendall(b'This is test message.')
data = client_sock.recv(1024)
client_sock.close()
