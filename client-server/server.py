# ---------------------------------------------------------------------------------------------------------------------
# Project:       Client-Server
# Description:
# Version:       v1.2.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

import socket


server_socket = socket.socket(socket.AF_INET,
                              socket.SOCK_STREAM,
                              proto=0)
server_socket.bind(('0.0.0.0', 8000))
server_socket.listen(5)

while True:
    client_socket, client_address = server_socket.accept()
    while True:
        data = client_socket.recv(1024)
        if not data:
            break
        client_socket.sendall(data)
    client_socket.close()
