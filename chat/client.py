# ---------------------------------------------------------------------------------------------------------------------
# Project:       Chat
# Description:
# Version:       v1.1.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

import socket
import threading
import time


KEY = 666

SHUTDOWN = False
JOIN = False


def receiving(name, sock):
    """Receive messages with encrypted."""
    while not SHUTDOWN:
        try:
            while True:
                data, addr = sock.recvfrom(1024)
                # print(data.decode('utf-8'))
                decrypt = ''
                k = False
                for i in data.decode('utf-8'):
                    if i == ':':
                        k = True
                        decrypt += i
                    elif not k or i == ' ':
                        decrypt += i
                    else:
                        decrypt += chr(ord(i) ^ KEY)
                print(decrypt)
                time.sleep(0.25)
        except Exception:
            pass


server = ('0.0.0.0', 9009)
client_sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
host = socket.gethostbyname(socket.gethostname())
port = 0
client_sock.bind((host, port))
client_sock.setblocking(False)  # catch errors

thread = threading.Thread(target=receiving, args=('RecvThread', client_sock))
thread.start()

alias = input('Enter username: ')


while not SHUTDOWN:
    if not JOIN:
        client_sock.sendto(('[' + alias + '] join chat ').encode('utf-8'),
                           server)
        JOIN = True
    else:
        try:
            message = input()
            crypt = ''
            for i in message:
                crypt += chr(ord(i) ^ KEY)
            message = crypt

            if message != '':
                client_sock.sendto(('[' + alias + '] :: ' + message).encode(
                    'utf-8'), server)
            time.sleep(0.25)
        except Exception:
            client_sock.sendto(('[' + alias + '] left chat ').encode(
                'utf-8'), server)
            SHUTDOWN = True


thread.join()  # check threads
client_sock.close()
