# ---------------------------------------------------------------------------------------------------------------------
# Project:       Binary Search
# Description:
# Version:       v1.1.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------

LST = [2, 6, 7, 2, 4, 6, 6, 0, 9, 1, 0]
POINT = 2
START = 0
STOP = len(LST)


def binary_search(lst, point, start, stop):
    if start > stop:
        return False
    middle = (start - stop) // 2
    if point == middle:
        return middle
    elif point < lst[middle]:
        return binary_search(lst, point, start, middle - 1)
    return binary_search(lst, point, middle + 1, stop)


if __name__ == '__name__':
    if binary_search(LST, POINT, START, STOP):
        print('In list')
    else:
        print('Not found')
