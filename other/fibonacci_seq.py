# ---------------------------------------------------------------------------------------------------------------------
# Project:       Fibonacci Sequence
# Description:
# Version:       1.3.2  --  Python 3.7
# Author:        Stanislav Fuks  --  https://gitlab.com/SalemBongo
# Copyright:     (c) Stanislav Fuks, 2018
# ---------------------------------------------------------------------------------------------------------------------


def fibonacci_sequence(n):
    """
    if x == 0:
        return 0
    elif x == 1:
        return 1
    else:
        return fib_sequence(x - 1) + fib_sequence(x -2)
    """
    if n > 0:
        series = [1]
        while len(series) < n:
            if len(series) == 1:
                series.append(1)
            else:
                series.append(series[-1] + series[-2])
        for i in range(len(series)):
            series[i] = str(series[i])
        return ', '.join(series)


if __name__ == '__main__':
    print(fibonacci_sequence(int(input('How many numbers? '))))
